# Glcd_EasyPIC_PRO_v7_Expansion_board

Resolving problem with GLCD display on the Mikroelektronika's EASYPIC PRO v7 development board.
Problem caused by selecting interrupt pins as controlling pin for GLCD display.

This problem solved using two ports which can be chosen by user.